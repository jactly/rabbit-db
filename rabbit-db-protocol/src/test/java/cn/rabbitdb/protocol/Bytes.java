/**
 * @(#) Bytes.java RabbitDB
 */
package cn.rabbitdb.protocol;

import java.util.Random;

import io.netty.buffer.ByteBuf;

/**
 * @author 智慧工厂@
 *
 */
public class Bytes {
	public static byte[] random(int length) {
		Random random = new Random(255);
		byte[] buffer = new byte[length];
		for (int i = 0; i < length; i++) {
			buffer[i] = (byte) random.nextInt();
		}
		return buffer;
	}

	public static void dumpBuffer(ByteBuf buf) {
		byte[] buffer = new byte[buf.readableBytes()];
		buf.readBytes(buffer);
		buf.resetReaderIndex();
		for (byte b : buffer) {
			System.out.print(b + ",");
		}
		System.out.println();
	}
}
