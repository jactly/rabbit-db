/**
 * 
 */
package cn.rabbitdb.protocol;

import cn.rabbitdb.protocol.mysql41.MySQLCapabilityFlags;
import cn.rabbitdb.protocol.mysql41.MySQLHandshark41;
import io.netty.buffer.ByteBuf;
import junit.framework.TestCase;

/**
 * MYSQL握手包測試
 * @author 智慧工厂@M
 *
 */
public class HandsharkPacketTestCase extends TestCase {
	public void testHandsharkPacket() {
		byte[] authPluginDataPart1 = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };
		byte[] authPluginDataPart2 = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
		MySQLHandshark41 handshark = new MySQLHandshark41(0, 10, "1.0.0", (int) Thread.currentThread().getId(),
				authPluginDataPart1, -15361, 33, 2, 21, authPluginDataPart2, "caching_sha2_password");
		ByteBuf buf = handshark.getMySQLMessageWriter().getBuffer();
		MySQLHandshark41 handshark2 = new MySQLHandshark41(buf);

		assertEquals(handshark.calucatePacketLength(), handshark2.getPacketLength());
		assertEquals(handshark.getAuthPluginName(), handshark2.getAuthPluginName());
		assertEquals(MySQLCapabilityFlags.CLIENT_PLUGIN_AUTH,
				handshark2.getCapabilityFlags() & MySQLCapabilityFlags.CLIENT_PLUGIN_AUTH);
	}
}
