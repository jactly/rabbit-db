/**
 * @(#) MySQLErrorPacket.java RabbitDB
 */
package cn.rabbitdb.protocol.mysql41;

import cn.rabbitdb.protocol.MySQLMessageReader;
import cn.rabbitdb.protocol.MySQLMessageWriter;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.ToString;

/**
 * @author win10
 *
 */
@ToString
@Getter
public class MySQLErrorPacket extends MySQLPacket {
	public static final byte FIELD_COUNT = (byte) 0xff;
	public static final byte SQLSTATE_MARKER = (byte) '#';
	public static final byte[] DEFAULT_SQLSTATE = "HY000".getBytes();

	private final int header;
	private final int errorCode;
	private final byte sqlStateMarker;
	private final String sqlState;
	private final String errorMessage;

	public MySQLErrorPacket(int packetId,int errorCode,String errorMessage) {
		super(0,packetId);
		this.header = 0xFF;
		this.errorCode = errorCode;
		this.sqlStateMarker = SQLSTATE_MARKER;
		this.sqlState = "HY000";
		this.errorMessage = errorMessage;
	}
	/**
	 * @param buffer
	 */
	public MySQLErrorPacket(int capabilityFlags, ByteBuf buffer) {
		super(buffer);
		MySQLMessageReader in = getReader();
		this.header = in.readUB() & 0xff;
		this.errorCode = in.readUB2();
		if ((capabilityFlags & MySQLCapabilityFlags.CLIENT_PROTOCOL_41) != 0) {
			this.sqlStateMarker = in.readUB();
			this.sqlState = new String(in.readBytes(5));
		} else {
			this.sqlStateMarker = 0;
			this.sqlState = null;
		}
		this.errorMessage = new String(in.readBytesEndwithEof());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.mysql.ddal.protocol.mysql41.MySQLPacket#getMySQLMessageWriter()
	 */
	@Override
	public MySQLMessageWriter getMySQLMessageWriter() {
		MySQLMessageWriter writer = new MySQLMessageWriter();
		this.setPacketLength(this.calucatePacketLength());
		writer.writeUB3(this.getPacketLength());
		writer.writeUB((byte)this.getPacketId());
		writer.writeByte((byte)header);
		writer.writeUB2(errorCode);
		writer.writeByte(sqlStateMarker);
		writer.writeBytes(sqlState.getBytes());
		if (errorMessage != null) {
			writer.writeBytes(errorMessage.getBytes());
		}
		return writer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.mysql.ddal.protocol.mysql41.MySQLPacket#calucatePacketLength()
	 */
	@Override
	public int calucatePacketLength() {
		int size = 8 +1;
		if (errorMessage != null) {
			size += errorMessage.length();
		}
		return size;
	}

}
