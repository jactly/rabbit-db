/**
 * @(#) MySQLOkPacket.java RabbitDB
 */
package cn.rabbitdb.protocol.mysql41;

import cn.rabbitdb.protocol.MySQLMessageReader;
import cn.rabbitdb.protocol.MySQLMessageWriter;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lombok.ToString;

/**
 * MySQL OK包
 * 
 * @author 智慧工厂@M
 *
 */
@ToString
public class MySQLOkPacket extends MySQLPacket {
	private byte header;
	private long affectedRows;
	private long lastInsertId;
	private int statusFlags;
	private int warinings;
	private String info;
	private int capability = 0;

	public MySQLOkPacket(int packetId, int capability, long affectedRows, long lastInsertId, int statusFlags,
			int warinings, String info) {
		super(0, packetId);
		this.header = (byte) 0x00;
		this.capability = capability;
		this.affectedRows = affectedRows;
		this.lastInsertId = lastInsertId;
		this.statusFlags = statusFlags;
		this.warinings = warinings;
		this.info = info;
	}

	public MySQLOkPacket(int capability, ByteBuf buffer) {
		super(buffer);
		this.capability = capability;
		MySQLMessageReader reader = this.getReader();
		header = reader.readUB();
		this.affectedRows = reader.readIntLenenc();
		this.lastInsertId = reader.readIntLenenc();
		if ((capability & MySQLCapabilityFlags.CLIENT_PROTOCOL_41) != 0) {
			this.statusFlags = reader.readUB2();
			this.warinings = reader.readUB2();
		}

		else if ((capability & MySQLCapabilityFlags.CLIENT_TRANSACTIONS) != 0) {
			this.statusFlags = reader.readUB2();
		}

		if ((capability & MySQLCapabilityFlags.CLIENT_SESSION_TRACK) != 0) {
			this.info = reader.readStringWithLength();
		} else {
			this.info = new String(reader.readBytesEndwithEof());
		}
	}

	@Override
	public MySQLMessageWriter getMySQLMessageWriter() {
		MySQLMessageWriter writer = new MySQLMessageWriter();
		this.calucatePacketLength();
		writer.writeUB3(this.getPacketLength());
		writer.writeUB((byte) this.getPacketId());
		writer.writeUB((byte) header);
		writer.writeLength(this.affectedRows);
		writer.writeLength(this.lastInsertId);
		if ((capability & MySQLCapabilityFlags.CLIENT_PROTOCOL_41) != 0) {
			System.out.println("写入:::");
			writer.writeUB2(this.statusFlags);
			writer.writeUB2(warinings);
		} else if ((capability & MySQLCapabilityFlags.CLIENT_TRANSACTIONS) != 0) {
			writer.writeUB((byte) statusFlags);
		}
		if ((capability & MySQLCapabilityFlags.CLIENT_SESSION_TRACK) != 0) {
			writer.writeStringWithLength(info);
		} else {
			writer.writeBytes(info.getBytes());
		}
		return writer;
	}

	@Override
	public int calucatePacketLength() {
		MySQLMessageWriter writer = new MySQLMessageWriter();
		int length = 0;
		length += 1;
		length += writer.getLength(affectedRows);
		length += writer.getLength(lastInsertId);
		if ((capability & MySQLCapabilityFlags.CLIENT_PROTOCOL_41) != 0) {
			length += 2;
			length += 2;
		} else if ((capability & MySQLCapabilityFlags.CLIENT_TRANSACTIONS) != 0) {
			length += 2;
		}

		length += this.info.getBytes().length;
		this.setPacketLength(length);
		System.out.println("####length::" + length);
		return length;
	}

	public static void main(String[] args) {
		byte[] bs = new byte[] { 7, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0 };
		ByteBuf buf = Unpooled.buffer();
		buf.writeBytes(bs);
		new MySQLOkPacket(0, 512, 0, 0, 0, 2, "");
		MySQLOkPacket ok = new MySQLOkPacket(MySQLCapabilityFlags.CLIENT_PROTOCOL_41, buf);
		System.out.println(ok);
	}
}
