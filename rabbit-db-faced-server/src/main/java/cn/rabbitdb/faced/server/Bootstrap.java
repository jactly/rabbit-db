/**
 * @(#) Bootstrap.java RabbitDB
 */
package cn.rabbitdb.faced.server;

import cn.rabbitdb.faced.server.configure.ConfigLoader;
import cn.rabbitdb.faced.server.configure.ServerConfig;

/**
 * @author 智慧工厂@
 *
 */
public class Bootstrap {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ServerConfig serverConfig = ConfigLoader.load(args);
		Lifecycle server = new DefaultFacedServer(serverConfig);
		server.startup();
	}

}
